package sales_management;

import util.Input;
import util.Output;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class OutputBill {
    public static ArrayList<Product> listProduct = new ArrayList<>();

    // tao arraylist hang hoa
    public static void createListProduct() throws FileNotFoundException {
        Scanner sc = Output.output("listProduct.txt");
        while (sc.hasNextLine()) {
            String iD = sc.nextLine();
            String name = sc.nextLine();
            String group = sc.nextLine();
            int price = Integer.parseInt(sc.nextLine());
            Product product = new Product(iD, name, group, price);
            listProduct.add(product);
        }
    }

    // im ra man hinh hoa don
    public static void display(String time,ArrayList<ProductNumber<String, Integer>> listProductBill ) {
        int moneyTotal = 0;
        System.out.println(time);
        System.out.printf("|---------------------|\n");
        for (ProductNumber ele : listProductBill) {
            System.out.printf("|%-17s %-3s|\n", ele.getObject1(), ele.getObject2());
            for (Product product : listProduct) {
                if (ele.getObject1().toString().compareTo(product.getName()) == 0) {
                    moneyTotal += (int)ele.getObject2() * product.getPrice();
                }
            }
        }
        System.out.println(moneyTotal);
        System.out.printf("|---------------------|\n");
    }

    public static void outputBill() throws FileNotFoundException {
        ArrayList<ProductNumber<String, Integer>> listProductBill = new ArrayList<>();
        System.out.print("nhap ID: ");
        String ID = Input.sc().nextLine();
        createListProduct();

        // tao Arraylist hang hoa trong bill
        Scanner sc = Output.output("bill" + ID + ".txt");
        String time = sc.nextLine();
        while (sc.hasNextLine()) {
            String product = sc.nextLine();
            int number = Integer.parseInt(sc.nextLine());
            ProductNumber ele = new ProductNumber(product, number);
            listProductBill.add(ele);
        }

        display(time, listProductBill);
        listProductBill.sort(null);
        display(time, listProductBill);

    }


}
