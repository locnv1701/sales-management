package sales_management;

import util.Output;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class OutputProduct {
    public static ArrayList<Product> listProduct = new ArrayList<>();

    public static void createList(Scanner sc) {
        while (sc.hasNextLine()) {
            String iD = sc.nextLine();
            String name = sc.nextLine();
            String group = sc.nextLine();
            int price = Integer.parseInt(sc.nextLine());
            Product product = new Product(iD, name, group, price);
            listProduct.add(product);
        }
    }
    public static void printList () {
        System.out.printf("|%-6s %-20s %-17s %13s|\n", "iD", "name", "group", "price");
        System.out.println("|-----------------------------------------------------------|");
        Output.genericDisplay(listProduct);
        System.out.println("|-----------------------------------------------------------|");
    }

    public static void outputProduct() {
        try {
            Scanner sc = Output.output("listProduct.txt");

            createList(sc);
            printList();

            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred");
            e.printStackTrace();
        }

    }
}