package sales_management;

import util.Input;
import validate.Validate;

import java.io.IOException;
import java.io.PrintWriter;

public class InputClient {

    public static void inputClient() throws IOException {

        PrintWriter out = Input.input("listClient.txt");

        System.out.print("Nhap ID: ");
        String ID = Input.sc().nextLine();
        System.out.print("Nhap ten: ");
        String name = Input.sc().nextLine();
        while (!Validate.validateName(name)) {
            System.out.println("nhap lai ten: ");
            name = Input.sc().nextLine();
        }
        System.out.print("Nhap dia chi: ");
        String address = Input.sc().nextLine();
        System.out.print("Nhap SDT: ");
        String phone = Input.sc().nextLine();
        while (!Validate.validatePhoneNumber(phone)) {
            System.out.println("nhap lai sdt: ");
            phone = Input.sc().nextLine();
        }

        Client client = new Client(ID, name, address, phone);
        out.println(ID);
        out.println(name);
        out.println(address);
        out.println(phone);
        System.out.println();
        System.out.println("nhap hang hoa va so luong: ");
        out.close();


        while (true) {
            System.out.print("nhap ten sp: ");
            String nameProduct = Input.sc().nextLine();
            if (nameProduct.compareTo("finish") == 0) {
                break;
            }
            if (!Validate.checkProductExist(nameProduct)) {
                System.out.println("Ten sp khong ton tai");
            } else {
                System.out.print("nhap so luong: ");
                int number = Integer.parseInt(Input.sc().nextLine());
                client.getBill().addItemToBill(nameProduct, number, client.getID());
            }
        }
    }
}
