package sales_management;

import util.Input;
import util.Time;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Bill {
    private String ID;
    private String time;
    private ArrayList<ProductNumber<String, Integer>> listProductBill;

    public ArrayList<ProductNumber<String, Integer>> getProductNumber() {
        return listProductBill;
    }

    public String getTime() {
        return this.time;
    }

    public String getID() {
        return ID;
    }

    public Bill(String ID) {
        this.time = Time.timeCurrent();

        try {
            File newBill = new File("source//src//file-text//bill" + ID + ".txt");
//            PrintWriter out = Input.input("bill" + ID + ".txt");
//            out.println(this.time);
            if (newBill.createNewFile()) {
                System.out.println("File created quinn: " + newBill.getName());
                PrintWriter out = Input.input(newBill.getName());
                out.println(this.time);
                out.close();
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void addItemToBill(String name, int number, String ID) throws IOException {

        PrintWriter out = Input.input("bill" + ID + ".txt");
        out.println(name);
        out.println(number);
        out.close();
    }

    public void displayBill() {
        System.out.println(this.time);
        System.out.println(this.ID);
        for (ProductNumber<String, Integer> productNumber : listProductBill) {
            System.out.println(productNumber.getObject1() + " " + productNumber.getObject2()); // in ra tat ca san pham va so luong
        }
    }


}
