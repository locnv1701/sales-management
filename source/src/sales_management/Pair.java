package sales_management;

class ProductNumber<T1, T2> implements Comparable<ProductNumber<T1, T2>> {

    private T1 obj1;
    private T2 obj2;

    public ProductNumber(T1 obj1, T2 obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public void setObj1(T1 obj1) {
        this.obj1 = obj1;
    }

    public void setObj2(T2 obj2) {
        this.obj2 = obj2;
    }

    public T1 getObject1() {
        return this.obj1;
    }

    public T2 getObject2() {
        return this.obj2;
    }



    @Override
    public int compareTo(ProductNumber<T1, T2> o) {
        return this.obj1.toString().compareTo(o.obj1.toString());
    }
}