package sales_management;

public class Product {
    private String iD;
    private String name;
    private String group;
    private int price;

    public void setID(String iD) {
        this.iD = iD;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public void setGroup(String group) {
        this.group = group;
    }
    public String getID() {
        return iD;
    }
    public String getName() {
        return name;
    }
    public int getPrice() {
        return price;
    }
    public String getGroup() {
        return group;
    }

    public Product() {
    }

    public Product(String iD, String name, String group, int price) {
        this.iD = iD;
        this.name = name;
        this.group = group;
        this.price = price;
    }

    public String toString(){
        return String.format("|%-6s %-20s %-17s %13s|\n", iD, name, group, price);
    }

}
