package sales_management;

import util.Input;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        int t = 1;
        while (t != -1) {
            System.out.println("Nhap 1 de in danh sach hang hoa");
            System.out.println("Nhap 2 de in danh sach khach hang");
            System.out.println("Nhap 3 de in hoa don");
            System.out.println("Nhap 4 de them hang hoa vao danh sach");
            System.out.println("Nhap 5 de them khach hang vao danh sach");

            t = Integer.parseInt(Input.sc().nextLine());
            switch (t) {
                case 1:
                    OutputProduct.outputProduct();
                    break;
                case 2:
                    OutputClient.outputClient();
                    break;
                case 3:
                    OutputBill.outputBill();
                    break;
                case 4:
                    InputProduct.inputProduct();
                    break;
                case 5://them khach hang vao danh sach, tao bill, nhap ten so luong sp mua
                    InputClient.inputClient();
                    break;
            }
        }



    }
}
