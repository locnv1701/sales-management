package sales_management;

import util.Input;

public class Client {
    private String iD;
    private String name;
    private String address;
    private String phone;
    private Bill bill;

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public String getID() {
        return this.iD;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Bill getBill() {
        return bill;
    }

    public Client() {
    }

    public Client(String ID, String name, String address, String phone) {
        this.iD = ID;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.bill = new Bill(ID);
    }

//    public void ClientAddItem(String nameProduct, int number){
//          this.bill.addItemToBill(nameProduct, number);
//    }

    private void printBill(){
        this.bill.displayBill();
    }

    public String toString() {
        return String.format("|%-6s %-25s %-17s %-13s|\n", this.iD, this.name, this.address, this.phone);
    }
}