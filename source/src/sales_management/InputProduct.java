package sales_management;

import util.Input;

import java.io.IOException;
import java.io.PrintWriter;

public class InputProduct {

    public static void inputProduct() throws IOException {
        try {
            PrintWriter out = Input.input("listProduct.txt");

            System.out.print("Nhap iD: ");
            String iD = Input.sc().nextLine();
            System.out.print("Nhap ten: ");
            String name = Input.sc().nextLine();
            System.out.print("Nhap nhom san phan: ");
            String group = Input.sc().nextLine();
            System.out.print("Nhap gia: ");
            long price = Long.parseLong(Input.sc().nextLine());

            out.println(iD);
            out.println(name);
            out.println(group);
            out.println(price);

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
