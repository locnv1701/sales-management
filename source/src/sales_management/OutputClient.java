package sales_management;

import util.Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class OutputClient {

    public static ArrayList<Client> listClient = new ArrayList<>();

    public static void createList(Scanner sc) {
        while (sc.hasNextLine()) {
            String iD = sc.nextLine();
            String name = sc.nextLine();
            String address = sc.nextLine();
            String phone = sc.nextLine();
            Client client = new Client(iD, name, address, phone);
            listClient.add(client);
        }
    }

    public static void printList() {
        System.out.printf("|%-6s %-25s %-17s %-13s|\n", "iD", "name", "address", "phone");
        System.out.println("|----------------------------------------------------------------|");
        Output.genericDisplay(listClient);
        System.out.println("|----------------------------------------------------------------|");
    }

    public static void outputClient() {
        try {
            Scanner sc = Output.output("listClient.txt");

            createList(sc);
            printList();

            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}