package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Output {

    public static Scanner output(String fileName) throws FileNotFoundException {
        String path = new File("source/src/file-text/" + fileName).getAbsolutePath();
        path = path.replaceAll("\\\\", "\\\\\\\\");
        File file = new File(path);
        Scanner sc = new Scanner(file);
        return sc;
    }

    public static <T> void genericDisplay(ArrayList<T> arrayList)
    {
        for(T ele : arrayList){
            System.out.println(ele.toString());
        }
    }

}
