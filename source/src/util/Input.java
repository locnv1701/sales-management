package util;

import java.io.*;
import java.util.Scanner;

public class Input {

    public static PrintWriter input(String fileName) throws IOException {

        String path = new File("source/src/file-text/" +fileName).getAbsolutePath();
        path = path.replaceAll("\\\\", "\\\\\\\\");
//        System.out.println(path);
        FileWriter fw = new FileWriter(path, true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);
        return out;
    }

    public static Scanner sc() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

}
