package validate;

import sales_management.Product;
import util.Output;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Validate {

    public static ArrayList<Product> listProduct = new ArrayList<>();

    // tao arraylist hang hoa
    public static void createListProduct() throws FileNotFoundException {
        Scanner sc = Output.output("listProduct.txt");
        while (sc.hasNextLine()) {
            String iD = sc.nextLine();
            String name = sc.nextLine();
            String group = sc.nextLine();
            int price = Integer.parseInt(sc.nextLine());
            Product product = new Product(iD, name, group, price);
            listProduct.add(product);
        }
    }

    public static boolean validateName(String in) {
        if (in.matches("[a-zA-Z\s]+"))
            return true;
        else
            return false;
    }

    public static boolean validateNumberOfProduct(String in) {
        if (in.matches("[0-9]{1,}"))
            return true;
        else
            return false;
    }

    public static boolean validatePhoneNumber(String in) {
        in = in.trim();
        if (in.matches("^0\\d{9}$"))
            return true;
        else
            return false;
    }

    public static boolean checkProductExist(String in) throws FileNotFoundException {
        createListProduct();
        for (Product product : listProduct) {
            if (in.compareTo(product.getName()) == 0) {
                return true;
            }
        }
        return false;
    }
}


